
## Casnic Control

Casnic Control is a hybrid desktop application written in the [Beysant](https://github.com/bitsii/beBase) programming language licensed under the [Mozilla Public License 2.0](https://www.mozilla.org/en-US/MPL/2.0/) - open source license which provides a application for controlling [Beysant Embedded](https://github.com/bitsii/beEmb) devices.

Hybrid Mobile Apps which include this codebase are available in the Google Play Store [Casnic Android App](https://play.google.com/store/apps/details?id=casnic.control&gl=US) and the Apple App Store [CasCon IOS App](https://apps.apple.com/us/app/cascon/id6458984046).

Instructions for using the application with your devices are available [here](https://gitlab.com/bitsii/CasCon/-/wikis/home).

## Getting Started

First - quick Getting Started! (more about Casnic Control below...)

Follow Getting Started for beApp recursively :-)  https://gitlab.com/bitsii/beApp

Then (from your shell / git shell on Windows)

git clone https://gitlab.com/bitsii/CasCon.git
cd CasCon
./scripts/devprep.sh

you must checkout in the same parent directory that contains beBase and beApp

### Building on desktop

Should work on Linux, Windows, and Mac.  Really only tested on Linux.

from in the project directory (CasCon) command line in a shell (git shell windows) run

../beApp/scripts/bldrunwajv.sh

(runs it too)

that's it

End of Getting Started!

## Be Careful

Use of this information and the software is entirely at your own risk.  As with any interaction between software and something "acting in the real world" or "running electricity around", be sure system failure or unexpected behavior cannot result in harm or injury to anyone.

## Credits

The official list of Casnic Control Authors:

Craig Welch <bitsiiway@gmail.com>
